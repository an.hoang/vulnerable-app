CREATE DATABASE IF NOT EXISTS mainDB;

USE mainDB;

CREATE TABLE IF NOT EXISTS mainTable (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    age INT,
    email VARCHAR(100),
    city VARCHAR(50)
);

LOAD DATA INFILE '/docker-entrypoint-initdb.d/sample_data.csv'
INTO TABLE mainTable
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

GRANT FILE ON *.* TO 'user0'@'%';
FLUSH PRIVILEGES;
