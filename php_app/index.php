<!DOCTYPE html>
<html>
<head>
    <title>Search Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container mt-5">
        <h2 class="mb-4">Search for a Value in the Database</h2>
        <form action="index.php" method="post" class="form-inline mb-4">
            <div class="form-group mr-2">
                <input type="text" name="search_input" class="form-control" placeholder="Enter a keyword" required>
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $keyword = $_POST["search_input"];

            $servername = "database";
            $username = "user0";
            $password = ##########;
            $dbname = "mainDB";
            $tablename = "mainTable";

            $conn = new mysqli($servername, $username, $password, $dbname);

            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM $tablename WHERE CONCAT(id, name, age, email, city) LIKE '%" . $keyword . "%'";
            $result = $conn->query($sql);

            if ($result) {
            // Check if any rows were returned
                if ($result->num_rows > 0) {
                    echo '<h2>Search results:</h2>';
                    echo '<table class="table table-bordered">';
                    echo '<thead class="thead-dark"><tr><th>ID</th><th>Name</th><th>Age</th><th>Email</th><th>City</th></tr></thead>';
                    echo '<tbody>';

                    while ($row = $result->fetch_assoc()) {
                        echo '<tr>';
                        echo '<td>' . $row['id'] . '</td>';
                        echo '<td>' . $row['name'] . '</td>';
                        echo '<td>' . $row['age'] . '</td>';
                        echo '<td>' . $row['email'] . '</td>';
                        echo '<td>' . $row['city'] . '</td>';
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                } else {
                    echo '<p>No results found.</p>';
                }
            } else {
                echo '<p>An error occurred while executing the query.</p>';
            }

            $conn->close();
        }
        ?>

    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
